import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_sample/components/card.dart';
import 'package:provider_sample/model/collection.dart';
import 'package:provider_sample/model/item.dart';

class Detail extends StatelessWidget {
  final Item item;

  Detail({required this.item});

  @override
  Widget build(BuildContext context) { 
    Collection collection = Provider.of<Collection>(context, listen: true);
    final _list = collection.items;
    //to update only if list is different

    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: Container(child: CardWidget(item)),
    );
  }
}
