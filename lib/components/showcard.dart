import 'package:flutter/material.dart';
import 'package:provider_sample/detail.dart';
import 'package:provider_sample/model/item.dart';

class ShowCardWidget extends StatelessWidget {
  final Item _item;

  ShowCardWidget(this._item);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          Icon(_item.favourite ? Icons.star : Icons.star_border),
          MaterialButton(
            onPressed: () {
              // https://docs.flutter.dev/cookbook/navigation/passing-data
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Detail(item: _item),
                ),
              );
            },
            child: Text(_item.name),
          ),
        ],
      ),
    );
  }
}
