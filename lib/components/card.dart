import 'package:flutter/material.dart';
import 'package:provider_sample/model/collection.dart';
import 'package:provider_sample/model/item.dart';
import 'package:provider/provider.dart';

class CardWidget extends StatelessWidget {
  final Item _item;

  CardWidget(this._item);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          IconButton(
              icon:
                  _item.favourite ? Icon(Icons.star) : Icon(Icons.star_border),
              onPressed: () {
                Provider.of<Collection>(context, listen: false)
                    .toggleFavourite(_item);
              }),
          Text(_item.name),
        ],
      ),
    );
  }
}
