import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:provider_sample/components/showcard.dart';
import 'package:provider_sample/model/collection.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => Collection(),
      builder: (context, child) => MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Collection collection = Provider.of<Collection>(context, listen: true);
    final _list = collection.items;

    return Scaffold(
      appBar: AppBar(
        title: Text("Movie List"),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: _list.length,
            itemBuilder: (_, index) => ShowCardWidget(_list[index])),
      ),
    );
  }
}
