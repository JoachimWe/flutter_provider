import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class Item {
  //simple Model class
  String name;
  bool favourite;

  Item({required this.name, required this.favourite});
}
