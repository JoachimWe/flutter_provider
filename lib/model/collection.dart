import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider_sample/model/item.dart';

class Collection with ChangeNotifier {
  List<Item> _items = [
    Item(name: "Game of Thrones", favourite: true),
    Item(name: "Startrek", favourite: false),
    Item(name: "Matrix", favourite: true),
    Item(name: "Star Wars", favourite: true),
    Item(name: "Mocking Jay", favourite: true),
  ];

  List<Item> get items => _items;

  toggleFavourite(Item item) {
    int index = _items.indexWhere((element) => element.name == item.name);
    _items[index].favourite = !_items[index].favourite;
    notifyListeners();
  }
}
